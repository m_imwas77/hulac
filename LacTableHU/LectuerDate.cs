﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacTableHU
{
    class LectuerDate
    {
        public string FromHouer { get; set; }
        public string ToHouer { get; set; }

        public IList<string> Days { get; set; }

        public string TimeToString()
        {
            string days = "";
            foreach (var item in Days)
            {
                days += item;
            }
            return "" + FromHouer + "" + ToHouer + "" + days;
        }
    }
}

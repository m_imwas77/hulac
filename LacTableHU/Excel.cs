﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
using _EXCEL = Microsoft.Office.Interop.Excel;
namespace LacTableHU
{
    class Excel
    {
        string path = "";
        _Application excel = new _EXCEL.Application();
        Workbook workbook;
       public  Worksheet worksheet;

        public Excel(string path , int sheet)
        {
            SetInfo(path, sheet);
        }

        public Excel()
        {

        }
        public void SetInfo(string path , int sheet)
        {
            this.path = path;
            workbook = excel.Workbooks.Open(path);

            worksheet = workbook.Worksheets[sheet];
        }
        public string ReadCell(int i , int j)
        {
            i++;j++;

            if (worksheet.Cells[i, j].Value2 != null)
            {
                return worksheet.Cells[i, j].Value2;
            }
            return "not found";
        }
        public void WriteToCell(int i , int j,string value)
        {
            i++;j++;
            worksheet.Cells[i, j].Value2 = value;
        }

        public void Save()
        {
            workbook.Save();
        }
        public void Colse()
        {
            workbook.Close();
        }
        public void SaveAs(string path)
        {
            workbook.SaveAs(path);
        }
    }
}

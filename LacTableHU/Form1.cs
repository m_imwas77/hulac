﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using HtmlAgilityPack;
using System.IO;
using Microsoft.Office.Interop.Excel;

using _EXCEL = Microsoft.Office.Interop.Excel;

namespace LacTableHU
{
    public partial class Form1 : Form
    {
        IList<LecturesTableRow> lecturesTableRows = new List<LecturesTableRow>();
        IList<string> RoomsNumber = new List<string>();
        Dictionary<string,int> RoomsIndexMap = new Dictionary<string, int>();
        Dictionary<string, int> TimeIndexMapSunday = new Dictionary<string, int>();
        Dictionary<String, int> TimeIndexMapMonday = new Dictionary<string, int>();
        string[] SundayTimes = { "08:00","09:00","10:00","11:00",
            "13:00","14:00","15:00","16:00"};
        string[] MondayTimes = { "08:00","09:30","11:00","13:00","14:30"};

        string[] SundayTimesFormat = { "8->9","9->10","10->11","11->12:15"
                ,"1->2","2->3","3->4","4->5"};
        string[] MondayFormat = { "8->9:30","9:30->11","11->12:30","1->2:30","2:30->4"};
        
        Excel excel;
        int RowCount = 0, ColumnCount = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e){        }

        private void WriteToExcelFile()
        {
            SetRoomsIndexToRoomsIndexMap();
            SetTimeIndexToTimeIndexMapSunday();
            SetTimeIndexToTimeIndexMapMonday();

            PrintRoomNumbersIntoExcelFile();


            PrintTimeFormatingForSundayIntoExcelFile();

            PrintTimeFormatingForMondaydayIntoExcelFile();

            PrintLectuerDetaelsIntoExcelFile();


            excel.Save();
            excel.Colse();
        }
        private void PrintLectuerDetaelsIntoExcelFile()
        {
            foreach (var item in lecturesTableRows)
            {
                int horizontal = TimeIndexMapSunday[item.Date.FromHouer];
                int vertical = RoomsIndexMap[item.RoomNumber];

                if (item.Date.Days[0] == "اثنين" || item.Date.Days[0] == "أثنين"
                    || item.Date.Days[0] == "اربعاء" || item.Date.Days[0] == "أربعاء")
                {
                    horizontal += 10;
                }

                if (item.Date.Days[0] == "خميس" || item.Date.Days[0] == "جمعه")
                {
                    continue;
                }

                excel.WriteToCell(horizontal,
                       vertical,
                       item.CourseName + "\n" +
                       item.CourseNumber + "\n" +
                       item.DivisionNumber + "ش" + "\n" +
                       item.TeacherName);
            }
        }
        private void PrintTimeFormatingForSundayIntoExcelFile()
        {
            RowCount = 1;
            foreach (var item in SundayTimesFormat)
            {
                excel.WriteToCell(RowCount++, 0, item);

            }
        }
        
        private void PrintTimeFormatingForMondaydayIntoExcelFile()
        {
            RowCount = 11;
            foreach (var item in MondayFormat)
            {
                excel.WriteToCell(RowCount++, 0, item);

            }
        }

        private void PrintRoomNumbersIntoExcelFile()
        {
            ColumnCount = 1;
            foreach (var room in RoomsNumber)
            {
                excel.WriteToCell(0, ColumnCount++, room);

            }
        }
        private void SetRoomsIndexToRoomsIndexMap()
        {
            int index = 0;
            foreach (var item in RoomsNumber)
            {
                RoomsIndexMap.Add(item,++index);
            }
        }

        private void SetTimeIndexToTimeIndexMapSunday()
        {
            int index = 0;
            foreach (var item in SundayTimes)
            {
                TimeIndexMapSunday.Add(item, ++index);
            }
        }

        private void SetTimeIndexToTimeIndexMapMonday()
        {
            int index = 0;
            foreach (var item in MondayTimes)
            {
                TimeIndexMapMonday.Add(item, ++index);
            }
        }

        
        private void InsertDataInToLectuerList(HtmlNodeCollection nodes)
        {
            if (nodes != null)
            {
                
                int repet = 0;
                LecturesTableRow row = new LecturesTableRow();
                foreach (HtmlAgilityPack.HtmlNode node in nodes)
                {
                    repet++;
                    if (repet == 1)
                    {
                        row.CollegeName = node.InnerText;
                    }
                    if (repet == 2)
                    {
                        row.CourseNumber = node.InnerText;
                    }
                    if (repet == 3)
                    {
                        row.DivisionNumber = node.InnerText;
                    }
                    if (repet == 4)
                    {
                        row.CourseName = node.InnerText;
                    }
                    if (repet == 5)
                    {
                        row.TeacherName = node.InnerText.Replace("&quot;", @"""");
                    }
                    if (repet == 6)
                    {
                        row.NumberOfStudents = node.InnerText;
                    }
                    if (repet == 7)
                    {
                        row.IdNumber = node.InnerText;
                    }
                    if (repet == 8)
                    {
                        IList<string> DaysList = new List<string>();
                        row.Date = new LectuerDate();
                        string Time = Convert.ToString(node.InnerText.ToString()).Trim();
                        string FromDate = "",ToDate= "", Days ="";
                        int i = 0;
                        foreach (var item in Time)
                        {
                            if (i >= 1 && i <= 5)
                            {
                                FromDate += item;
                            }
                            else if (i >= 7 && i <= 11)
                            {
                                ToDate += item;
                            }
                            else if(i>11 && i< Time.Length) Days += item;
                            i++;
                        }
                        Days = Days.Replace(")", "");
                        Days = Days.Replace("(", "");
                        Days = Days.Trim();

                        DaysList = Days.Split(',');
                        row.Date.FromHouer = ToDate;
                        row.Date.ToHouer = FromDate;
                        row.Date.Days = DaysList;
                    }
                    if (repet == 9)
                    {
                        row.RoomNumber = node.InnerText;
                        //insert room number into RoomsNumber list
                        RoomsNumber.Add(node.InnerText);
                    }
                    if (repet == 10)
                    {
                        repet = 0;
                        lecturesTableRows.Add(new LecturesTableRow
                        {
                            CollegeName = row.CollegeName,
                            CourseNumber = row.CourseNumber,
                            DivisionNumber = row.DivisionNumber,
                            CourseName = row.CourseName,
                            TeacherName = row.TeacherName,
                            NumberOfStudents = row.NumberOfStudents,
                            IdNumber = row.IdNumber,
                            Date = row.Date,
                            RoomNumber = row.RoomNumber
                        }
                        );
                    }
                }

                lecturesTableRows.RemoveAt(lecturesTableRows.Count - 1);
                lecturesTableRows = lecturesTableRows.Distinct().ToList();
                // delete the extra data form RoomsNumber list 
                RoomsNumber.RemoveAt(RoomsNumber.Count - 1);
                // make RoomsNumber list distinct
                RoomsNumber = RoomsNumber.Distinct().ToList();
                
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if(txtParseer.Text == "")
            {
                MessageBox.Show("empty string :)");
                return;
            }
            try
            {
                InsertDataInToLectuerList(RowsElement(txtParseer.Text.ToString()));
                txtParseer.Text = "";
                MessageBox.Show("Data inserted :)");
            }
            catch (Exception)
            {
                MessageBox.Show("Error Hapend check Your interd Data or clear");
            }
        }

        private void btnCreate_Click(object sender, EventArgs e)
        {
            string excelFilePath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/lectuersTimeTable.xlsx";
          

            try
            {
                excel = new Excel(excelFilePath, 1);
                WriteToExcelFile();
               MessageBox.Show("File Created :)");
            }
           catch (Exception)
           {
               MessageBox.Show("Error Hapend check Your interd Data or clear");
           } 
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            lecturesTableRows.Clear();
            RoomsIndexMap.Clear();
            RoomsNumber.Clear();
            txtParseer.Text = "";
            MessageBox.Show("Data Clerd :)");
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private HtmlAgilityPack.HtmlNodeCollection RowsElement(string htmlText)
        { 
            var htmldoc = new HtmlAgilityPack.HtmlDocument();

            htmldoc.LoadHtml(htmlText);

            var table = htmldoc.DocumentNode.SelectSingleNode("//table[@id='GridView1']");
            
            htmldoc.LoadHtml(table.InnerHtml);
            var tdNodes = htmldoc.DocumentNode.SelectNodes("//td");
            return tdNodes;
        }


    }
}

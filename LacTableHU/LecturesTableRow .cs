﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LacTableHU
{
    class LecturesTableRow
    {
        public string CollegeName { get; set; }
        public string CourseNumber { set; get; }
        public string DivisionNumber { set; get; }
        public string CourseName { set; get; }
        public string TeacherName { set; get; }
        public string NumberOfStudents { set; get; }
        public string IdNumber { set; get; }
        public LectuerDate Date { set; get; }
        public string RoomNumber { set; get; }

        public string DataToText()
        {
            return (CollegeName + Environment.NewLine + CourseName + Environment.NewLine +
                DivisionNumber + Environment.NewLine + CourseNumber  + Environment.NewLine +
                TeacherName + Environment.NewLine + NumberOfStudents + Environment.NewLine +
                IdNumber + Environment.NewLine + Date.TimeToString() + Environment.NewLine + RoomNumber + Environment.NewLine +
                "==================================" + Environment.NewLine);
        }

        
    }
    
}
